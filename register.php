<?php

use \Flood\Captn;

if (function_exists('captnIsSteering') && captnIsSteering()) {
    Captn\Acknowledge::signal(
        'flood.i18n',
        'root.flood.i18n',
        Captn::TYPE__COMPONENT,
        [
            /*'annotation' => [
                // which folders should be scanned
                'scan' => [
                    [
                        'ext'     => 'php',
                        'include' => __DIR__ . '/src/',
                    ],
                ],
            ],*/
        ]
    );
}