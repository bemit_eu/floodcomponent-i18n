<?php

namespace Flood\Component\I18n;


class Dictionary {

    protected static $read = [];
    public static $debug = false;

    const SEPARATOR = '.';

    protected $store = [];

    public function read($path, $selector) {
        if (!in_array($selector, static::$read)) {
            if (is_file($path)) {
                $segment = json_decode(file_get_contents($path), true);
                foreach ($segment as $sel => $val) {
                    $this->store[$selector . '.' . $sel] = $val;
                }
                static::$read[] = $selector;
            } else if (static::$debug) {
                error_log('Flood Dictionary: File could not be found: ' . $path);
            }
        }
    }

    protected function lexText($text) {
        $arr = explode(static::SEPARATOR, $text);

        return [
            'namespace' => array_shift($arr),
            'element'   => array_pop($arr),
            'path'      => $arr,
        ];
    }

    public function translate($text) {
        return $this->store[$text];
    }

    /**
     * @param $text
     *
     * @return bool
     */
    public function has($text) {
        return array_key_exists($text, $this->store);
    }
}