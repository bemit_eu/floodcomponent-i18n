<?php

namespace Flood\Component\I18n;


use Flood\Component\I18n\Model\Country;
use Hydro\Container;

class I18n extends Dictionary {

    protected static $read = [];

    protected $code;

    protected $country;

    public function __construct($code) {
        $this->code = $code;
        $this->country = new Country($code);
    }

    /**
     * @param $text
     *
     * @return bool
     */
    public function has($text) {
        if (parent::has($text)) {
            return parent::has($text);
        }
        $selector = $this->lexText($text);
        foreach (Container::_i18n()->getDictionaryDir($selector['namespace']) as $dir) {
            $file = $dir . '/' . implode('/', $selector['path']) . '/' . $this->code . '.json';
            $this->read(
                $file,
                $selector['namespace'] . '.' . implode('.', $selector['path'])
            );
        }

        return array_key_exists($text, $this->store);
    }
}