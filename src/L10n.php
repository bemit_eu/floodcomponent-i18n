<?php

namespace Flood\Component\I18n;


use Flood\Component\I18n\Model\Language;
use Hydro\Container;

class L10n extends Dictionary {

    protected static $read = [];

    protected $code;

    protected $lang;

    public function __construct($code) {
        $this->code = $code;
        $this->lang = new Language($code);
    }

    /**
     * @param $text
     *
     * @return bool
     */
    public function has($text) {
        if (parent::has($text)) {
            return parent::has($text);
        }
        $selector = $this->lexText($text);
        foreach (Container::_i18n()->getDictionaryDir($selector['namespace']) as $dir) {
            $file = $dir . '/l10n/' . implode('/', $selector['path']) . '/' . $this->code . '.json';
            $this->read(
                $file,
                $selector['namespace'] . '.' . implode('.', $selector['path'])
            );
        }

        return array_key_exists($text, $this->store);
    }
}