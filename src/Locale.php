<?php

namespace Flood\Component\I18n;

/**
 * The Locale Class
 *
 * Stores and represents one item of a i18n/l10n combination. Is generated with the $locale from within the localestorage
 *
 * @package Flood\Component\I18n
 */
class Locale {

    protected $code;

    /**
     * @var \Flood\Component\I18n\I18n
     */
    public $i18n;
    /**
     * @var \Flood\Component\I18n\L10n
     */
    public $l10n;

    public function __construct($locale) {
        $this->code = $this->lexLocale($locale);
        $this->code['locale'] = $locale;
        $this->i18n = new I18n($this->code['country_code']);
        $this->l10n = new L10n($this->code['lang_code']);;
    }

    /**
     * @param $locale
     *
     * @return array|bool
     */
    protected function lexLocale($locale) {
        $seperator_pos = 0;
        if (false !== ($seperator_pos_tmp = strpos($locale, '-'))) {
            $seperator_pos = $seperator_pos_tmp;
        } else if (false !== ($seperator_pos_tmp = strpos($locale, '_'))) {
            $seperator_pos = $seperator_pos_tmp;
        } else {
            return false;
        }

        return [
            'lang_code'    => substr($locale, 0, $seperator_pos),
            'country_code' => substr($locale, $seperator_pos + 1),
        ];
    }

    /**
     * @param $text
     *
     * @return string
     */
    public function translate($text) {
        if ($this->i18n->has($text)) {
            return $this->i18n->translate($text);
        } else if ($this->l10n->has($text)) {
            return $this->l10n->translate($text);
        } else {
            return '';
        }
    }


}