<?php

namespace Flood\Component\I18n;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * CDN - json config based routing for file and folder serving
 *
 * @Service(id="i18n", executed=false)
 *
 */
class LocaleStorage {

    /**
     * @var array|\Flood\Component\I18n\Locale
     */
    protected $active = [];

    protected $dictionary_dir_list = [];

    protected $locale_store = [];


    /**
     * @param string $locale possible: * or an ISO-639 + ISO-3166 combination, concated with an underscore _ or dash -
     */
    public function setActive($locale) {
        $this->setStore($locale);
        $this->locale_store['active'] = &$this->locale_store[$locale];
    }

    /**
     * @param string $id the locale or an id like 'active'
     *
     * @return Locale|null
     */
    public function getStore($id) {
        if (isset($this->locale_store[$id])) {
            return $this->locale_store[$id];
        }

        return null;
    }

    public function setStore($locale) {
        if (!isset($this->locale_store[$locale])) {
            $this->locale_store[$locale] = new Locale($locale);
        }
    }

    /**
     * @param        $text
     * @param string $to locale
     *
     * @return string
     */
    public function translate($text, $to = null) {
        if (null === $to) {
            $to = $this->getStore('active');
        } else {
            if (null === $this->getStore($to)) {
                $this->setStore($to);
            }
            $to = $this->getStore($to);
        }

        return $to->translate($text);
    }

    public function addDictionaryDir($dir, $namespace) {
        $this->dictionary_dir_list[$namespace][] = $dir;
    }

    /**
     * @param $namespace
     *
     * @return array
     */
    public function getDictionaryDir($namespace) {
        if (isset($this->dictionary_dir_list[$namespace])) {
            return $this->dictionary_dir_list[$namespace];
        } else {
            return [];
        }
    }
}